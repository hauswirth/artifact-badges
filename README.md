# Artifact Badges
The goal of this repository is to share the artifact badge files with people in the community (e.g., AEC chairs) who need to issue such badges.

If you are an author of a paper, please get your badge from your AEC chair. They most probably need to approve of it.

If you are an AEC chair for a conference for which there's already a set of badges in here, feel free to use the PDFs of your conference's badges. If there are no PDFs for your conference, please contact Matthias Hauswirth and he will be glad to create and add them.


### Original AEC Badge
The original AEC badges were created by Matthias Hauswirth and first used for OOPSLA 2013,
the first OOPSLA conference with an artifact evaluation committee (AEC).
More information about the OOPSLA'13 AEC is available in the
"[Artifact Evaluation Artifact](http://evaluate.inf.usi.ch/artifacts/aea)".

The badges were designed to be used for papers that passed an artifact evaluation
following Shriram Krishnamurthi's model, documented at [http://www.artifact-eval.org/](http://www.artifact-eval.org/).
Matthias and others involved in past AE efforts 
were subsequently contacted by several new AEC chairs to provide badges for their conferences,
and Matthias produced them whenever the conference used an artifact evaluation process that closely followed Shriram's original idea.
Because each badge contains the name of the conference (but not the year), 
the first time a conference wanted to use a badge,
such a badge had to be derived from the source (which Matthias had).

### ACM Pubs Board Compatible Badges
In 2016, the ACM publications board introduced "[Result and Artifact Review and Badging](http://www.acm.org/publications/policies/artifact-review-badging)" standards,
which are close to Shriram's model in spirit, but deviate slightly and extend it.

When the ICFP'17 AEC chairs asked about badges and their relationship to the ACM pubs board standards,
Matthias created a new set of badges that closely follow the ACM pubs board standards.
The old badges exactly reflect Shriram's model, and they are not 100% consistent with the ACM pubs board standards.

The pubs board standards [introduces](http://www.acm.org/publications/policies/artifact-review-badging) 
three separate badges ("Artifacts Evaluated", "Artifacts Available", and "Results Validated"),
and two of those badges come in two different levels.
Matthias created five new badges to closely reflect this.
The sources of these badges are available in this Git repository.

**Note**: The [2017 ACM Master Article Template](http://www.acm.org/publications/proceedings-template)
contains an "ae-logo.pdf" file that is based on Matthias Hauswirth's original AEC badge design (but with changed fonts).
It's probably better to use the new badges in this repository instead.



## Badges
Do not use these (300x300 pixel) PNG images. Use the PDFs when including badges in your paper.

### Artifacts Evaluated
The AEC of the respective conference (e.g., ICFP) decides whether it gives one of these two badges to a given paper (see [ACM pubs board standards](http://www.acm.org/publications/policies/artifact-review-badging) for their meaning):

![Artifacts Evaluated: Functional](Badge-ArtifactsEvaluated-Functional-ICFP.png)
![Artifacts Evaluated: Reusable](Badge-ArtifactsEvaluated-Reusable-ICFP.png)


### Artifacts Available
The AEC of the respective conference (e.g., ICFP) decides whether it gives this badge to a given paper (see [ACM pubs board standards](http://www.acm.org/publications/policies/artifact-review-badging) for its meaning):

![Artifacts Available](Badge-ArtifactsAvailable-ICFP.png)


### Results Validated
These badges can only be given to a paper *after* it has been published, and after someone else has reproduced or replicated the results successfully. The reproduction or replication probably was published in a different conference or journal than the original paper that is to receive a badge, thus these badges do not include the conference name (see [ACM pubs board standards](http://www.acm.org/publications/policies/artifact-review-badging) for their meaning):

![Results Validated: Replicated](Badge-ResultsValidated-Replicated.png)
![Results Validated: Reproduced](Badge-ResultsValidated-Reproduced.png)



## Implementation Details
This repository contains the source code (InkScape .svg files) of the artifact badges, and it contains PNG (300x300 pixels) and PDF versions exported from InkScape. One should use the PDF when including a badge in a paper. The PNG is useful for web sites (like the ACM DL, or conf.researchr.org).

The badges were drawn in [InkScape](https://inkscape.org/en/) version 0.91 on macOS. The badges use the font "Nanum Gothic" (regular and bold), which is included in Mac OS X 10.7 Lion.

If you decide not to ask Matthias Hauswirth to create badges for your conference, but you decide to create the badges yourself, please ensure that you use a process in close agreement with the [ACM Publications board standards](http://www.acm.org/publications/policies/artifact-review-badging). When editing the files, make sure you use the same font, only replace the conference name, adjust the whitespace in the circular text if needed, and ensure the circular text is tilted to the left about 12 degrees (see the guidelines in InkScape).